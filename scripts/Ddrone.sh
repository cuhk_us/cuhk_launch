# Please run with sudo.
# This bash will launch all the components of the system, in the order of:
# 0. px4 (mavros & gcs_bridge) 

init_path=/home/$USER/catkin_ws
export LC_ALL="en_US.UTF-8"

# delay between launching various modules
module_delay=1.5

# check whether is running as sudo
if [ "$EUID" -eq 0 ]
    then echo "Please DO NOT run as root."
    exit
fi

if [ "${STY}" != "" ]
    then echo "You are running the script in a screen environment. Please quit the screen."
    exit
fi

output="$(screen -ls)"
if [[ $output != *"No Sockets found"* ]]; then
    echo "There are some screen sessions alive. Please run 'pkill screen' before launching uavos."
    exit
fi

echo "The system is booting..."

# roscore
screen -d -m -S roscore bash -c "source devel/setup.bash; roscore; exec bash -i"
sleep ${module_delay}
sleep ${module_delay}
sleep ${module_delay}
echo "roscore ready."

screen -d -m -S rosparam bash -c "source devel/setup.bash; source /opt/ros/kinetic/setup.bash; exec bash -i"
echo "rosparam ready."

# --------------------------------------------
# prepareation
cd ${init_path}

#################################################################################################################################
# 0. px4
screen -d -m -S px4 bash -c "source devel/setup.bash; roslaunch mavros px4.launch; exec bash -i"
sleep ${module_delay}
echo "px4 ready."

#################################################################################################################################
# 1. reference_generator
screen -d -m -S reference_generator bash -c "source devel/setup.bash; roslaunch nus_reference_generator reference_generator.launch; exec bash -i"
sleep ${module_delay}
echo "reference_generator ready."

#################################################################################################################################
# 2. task_manager
screen -d -m -S task_manager bash -c "source devel/setup.bash; roslaunch nus_task_manager task_manager.launch; exec bash -i"
sleep ${module_delay}
echo "task_manager ready."

#################################################################################################################################
# 3. pub_uav_state
screen -d -m -S pub_uav_state bash -c "source devel/setup.bash; roslaunch pub_uav_state pub_uav_state.launch; exec bash -i"
sleep ${module_delay}
echo "pub_uav_state ready."

#################################################################################################################################
# 4. uwb
#screen -d -m -S uwb bash -c "source devel/setup.bash; roslaunch uwb_p440 uwb_p440_science_center.launch; exec bash -i"
#sleep ${module_delay}
#echo "uwb ready."

#################################################################################################################################
# 4. zed
screen -d -m -S zed bash -c "source devel/setup.bash; roslaunch zed_wrapper zed.launch; exec bash -i"
sleep ${module_delay}
sleep ${module_delay}
sleep ${module_delay}
sleep ${module_delay}
sleep ${module_delay}
sleep ${module_delay}
sleep ${module_delay}
sleep ${module_delay}
sleep ${module_delay}
sleep ${module_delay}
sleep ${module_delay}
sleep ${module_delay}
echo "zed ready."

#################################################################################################################################
# 5. ssf_updates
screen -d -m -S ssf_vio bash -c "source devel/setup.bash; roslaunch ssf_updates visionpose_sensor_ddrone.launch; exec bash -i"
echo "ssf_ekf ready"
sleep ${module_delay}

#################################################################################################################################
# 6. output path 
#screen -d -m -S path bash -c " source devel/setup.bash; roscd nus_launch/scripts ; python show_path.py; exec bash -i"
#sleep ${module_delay}

#############################################################################################################################
echo "System is started."

