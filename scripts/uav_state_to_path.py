#!/usr/bin/env python
 
import rospy
from std_msgs.msg import String
from nav_msgs.msg import Odometry, Path
from geometry_msgs.msg import PoseWithCovarianceStamped, PoseStamped
from common_msgs.msg import state
import time


def callback(data):
        global xAnt
        global yAnt
        global cont

        pose = PoseStamped()

        pose.header.frame_id = "map"
        pose.pose.position.x = data.pos.x
        pose.pose.position.y = data.pos.y
        pose.pose.position.z = data.pos.z
        pose.pose.orientation.x = 0.0
        pose.pose.orientation.y = 0.0
        pose.pose.orientation.z = 0.0
        pose.pose.orientation.w = 1.0

        if (xAnt != pose.pose.position.x or yAnt != pose.pose.position.y):
                pose.header.seq = path.header.seq + 1
                path.header.frame_id = "map"
                path.header.stamp = rospy.Time.now()
                pose.header.stamp = path.header.stamp
                path.poses.append(pose)
        cont = cont + 1

        rospy.loginfo("Hit: %i" % cont)
        #rospy.loginfo("pos.x: %i" %data.pos.x )
        if cont > max_append:
                path.poses.pop(0)

        pub.publish(path)

        xAnt = pose.pose.orientation.x
        yAnt = pose.pose.position.y
        return path


if __name__ == '__main__':
        # Initializing global variables
        global xAnt
        global yAnt
        global cont
        xAnt = 0.0
        yAnt = 0.0
        cont = 0

        # Initializing node
        rospy.init_node('path_plotter')
        max_append = rospy.set_param("~max_list_append", 1000)
        max_append = 1000

        pub = rospy.Publisher('/path', Path, queue_size=1)

        path = Path() 
        msg = state()

        msg = rospy.Subscriber('/rt_ref_gen/current_state', state, callback)

        rate = rospy.Rate(10)   

        try:
                while not rospy.is_shutdown():
                    # rospy.spin()
                    rate.sleep()
        except rospy.ROSInterruptException:
                pass
